/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   encryption2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 16:01:40 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 16:01:43 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void		make_it_butiful_decryption(uint8_t **rez, ssize_t *hm)
{
	size_t pad;
	size_t count;
	size_t count2;

	count = ft_strlenuu(*rez) - 1;
	count2 = 0;
	pad = (*rez)[count];
	if (pad < 1 || pad > 8)
		exit(1);
	while (count > 0 && (*rez)[count] == pad)
	{
		count--;
		count2++;
	}
	if (count2 != pad)
		exit(1);
	count = ft_strlenuu(*rez) - 1;
	while (count2 > 0)
	{
		(*rez)[count--] = '\0';
		(*hm)--;
		count2--;
	}
}

void		i_luv_norm(t_input *inp, uint64_t keys[16], t_list **head,
					size_t *pos)
{
	uint8_t		tmp[9];
	uint8_t		*hm;

	fill_w_pad(tmp, 0, inp, pos);
	ft_lst_push_back(head, (hm = encrypt_d(keys, tmp)), 8);
	free(hm);
	fill_w_pad(tmp, 8, inp, pos);
	ft_lst_push_back(head, (hm = encrypt_d(keys, tmp)), 8);
	free(hm);
}
