/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_cbc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 17:18:29 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 17:18:35 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

uint8_t		*encrypt_d_c(uint64_t keys[16], uint8_t tmp[9], t_options *options)
{
	int			count;
	uint64_t	rez;
	uint64_t	pairs[17][2];

	rez = 0;
	initialize_rez(&rez, tmp);
	initial_vector(options, &rez, 0, 0);
	count = 1;
	first_permutation_of_data(&rez);
	pairs[0][0] = (rez & (MASK_D << 32)) >> 32;
	pairs[0][1] = rez & MASK_D;
	while (count <= 16)
	{
		pairs[count][0] = pairs[count - 1][1];
		pairs[count][1] = pairs[count - 1][0] ^ function_f(keys[count -
		1], pairs[count - 1][1]);
		count++;
	}
	rez = (pairs[16][1] << 32) | pairs[16][0];
	finelest_permutation(&rez);
	options->bl = rez;
	return (transform_to_uint_8(rez));
}

void		checking_upon_null_input_c(t_input *inp, t_options *options,
									uint64_t keys[16], t_list **head)
{
	size_t	pos;
	uint8_t tmp[9];

	pos = 0;
	if (head && inp->size == 0)
	{
		fill_w_pad(tmp, 8, inp, &pos);
		ft_lst_push_back(head, encrypt_d_c(keys, tmp, options), 8);
	}
}

void		encrypt_des_c(t_input *inp, t_options *options, uint64_t keys[16],
						t_list **head)
{
	size_t		pos;
	uint8_t		tmp[9];
	uint8_t		*hm;

	pos = 0;
	hm = NULL;
	checking_upon_null_input_c(inp, options, keys, head);
	while (inp->size > 0)
	{
		free(hm);
		if (inp->size > 8)
			fill_w_pad(tmp, 0, inp, &pos);
		else if (inp->size < 8)
			fill_w_pad(tmp, 8 - inp->size, inp, &pos);
		else
		{
			fill_w_pad(tmp, 0, inp, &pos);
			ft_lst_push_back(head, (hm = encrypt_d_c(keys, tmp, options)), 8);
			free(hm);
			fill_w_pad(tmp, 8, inp, &pos);
		}
		ft_lst_push_back(head, (hm = encrypt_d_c(keys, tmp, options)), 8);
	}
	free(hm);
}

void		process_to_ciph_d_c(t_options *options, uint64_t keys[16],
							t_input *hm)
{
	t_list *head;

	head = NULL;
	if (options->d)
		decrypt_des_c(hm, options, keys, &head);
	else
		encrypt_des_c(hm, options, keys, &head);
	output_des(&head, *options, hm);
	if (!options->a || options->d)
	{
		free(hm->s);
		free(hm);
	}
}

int			make_des_cbc(char **av, int ac)
{
	t_options	options;
	uint64_t	keys[16];
	t_input		*inp;

	initialize_options(&options);
	read_options(av, &options, ac);
	check_options_des_cbc(&options);
	if (!(inp = read_input(options.in)))
		exit(1);
	form_keys(options, keys);
	process_to_ciph_d_c(&options, keys, inp);
	free(options.hex);
	return (0);
}
