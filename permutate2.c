/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   permutate2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 16:52:37 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 16:52:40 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void	permutation_after_sboxes(uint64_t *rez)
{
	size_t		count;
	uint64_t	tmp;
	uint64_t	tmp2;
	uint64_t	tmp_rez;
	static int	arr[32] = {16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5,
	18, 31, 10, 2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25};

	count = 0;
	tmp2 = 0;
	tmp_rez = 31;
	while (count < 32)
	{
		tmp = (*rez & ((uint64_t)1 << (32 - arr[count])));
		tmp2 |= (tmp >> (32 - arr[count])) << tmp_rez;
		count++;
		tmp_rez--;
	}
	*rez = tmp2;
}

void	first_permutation_of_data(uint64_t *rez)
{
	size_t		count;
	uint64_t	tmp;
	uint64_t	tmp2;
	uint64_t	tmp_rez;
	static int	arr[64] = {58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28,
	20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57,
	49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29,
	21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7};

	count = 0;
	tmp2 = 0;
	tmp_rez = 63;
	while (count < 64)
	{
		tmp = (*rez & ((uint64_t)1 << (64 - arr[count])));
		tmp2 |= (tmp >> (64 - arr[count])) << tmp_rez;
		count++;
		tmp_rez--;
	}
	*rez = tmp2;
}
