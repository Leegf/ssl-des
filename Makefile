# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/06 12:02:16 by lburlach          #+#    #+#              #
#    Updated: 2018/03/06 13:24:33 by lburlach         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

NAME = ft_ssl
OBJ =  add_funcs.o add_funcs2.o base64.o base64_2.o base64_3.o base_64_4.o decryption.o des.o des3.o des3_2.o des3_3.o des3_cbc.o des3_cbc2.o des_cbc.o des_cbc2.o des_options.o encryption.o encryption2.o keys.o main.o permutate2.o permutation.o read_options.o read_options2.o
CC = gcc
CFLAGS = -Wall -Wextra -Werror
CURDIR = libft
HEAD = ssl_des.h
INC = $(addsuffix /includes, $(CURDIR))

all: $(NAME)
	$(MAKE) -C $(CURDIR)

$(NAME): $(OBJ)
	$(MAKE) -C $(CURDIR)
	$(CC) $(CFLAGS) -I $(INC) -o $@ $(CURDIR)/$(addsuffix .a, $(CURDIR)) $^

libft.a: ololo
	$(MAKE) -C $(CURDIR)/

ololo:
	true

clean:
	rm -f $(OBJ)
	$(MAKE) -C $(CURDIR) clean

fclean: clean
	rm -f $(NAME)
	$(MAKE) -C $(CURDIR) fclean

re: fclean all

$(OBJ) : %.o: %.c
	$(CC) -I $(INC) -c $(CFLAGS) $< -o $@

$(OBJ): $(HEAD)
