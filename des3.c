/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des3.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 10:44:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/28 10:44:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void	decrypt_des3(t_input *inp, t_options options, uint64_t keys[16],
					t_list **head)
{
	uint8_t	tmp[9];
	uint8_t	*hm;
	size_t	pos;
	size_t	rem;

	pos = 0;
	hm = NULL;
	if (options.d)
		check_whether_base64_is_on(inp, options);
	rem = inp->size;
	while (inp->size > 0)
	{
		free(hm);
		fill_tmp(tmp, inp, &pos);
		ft_lst_push_back(head, (hm = decrypt_d(keys, tmp)), 8);
		if (rem - pos < 8)
			break ;
	}
	free(hm);
}

void	process_to_ciph_d3(t_options options, uint64_t keys[16], t_input *hm,
						uint64_t hex_3[4])
{
	if (options.d)
		des3_e(options, keys, hm, hex_3);
	else
		des3_d(options, keys, hm, hex_3);
	if (!options.a || options.d)
	{
		free(hm->s);
		free(hm);
	}
}

int		make_des3_ecb(char **av, int ac)
{
	t_options	options;
	uint64_t	keys[16];
	uint64_t	hex_3[4];
	t_input		*inp;

	hex_3[3] = '\0';
	initialize_options(&options);
	read_options(av, &options, ac);
	check_options_des3(&options, hex_3);
	if (!(inp = read_input(options.in)))
		exit(1);
	form_keys3(keys, hex_3, 0);
	process_to_ciph_d3(options, keys, inp, hex_3);
	return (0);
}
