/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des3_3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 19:40:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 19:40:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

static int g_ultra_flag;

void	des3_d(t_options options, uint64_t keys[16], t_input *hm,
			uint64_t hex_3[4])
{
	t_list *head;

	head = NULL;
	encrypt_des3(hm, keys, &head);
	free(hm->s);
	hm->size = (size_t)u_allocate(head, &(hm->s));
	ft_lst_clear(&head);
	form_keys3(keys, hex_3, 1);
	decrypt_des3(hm, options, keys, &head);
	free(hm->s);
	hm->size = (size_t)u_allocate(head, &(hm->s));
	ft_lst_clear(&head);
	form_keys3(keys, hex_3, 2);
	g_ultra_flag = 1;
	encrypt_des3(hm, keys, &head);
	output_des(&head, options, hm);
}

void	des3_e(t_options options, uint64_t keys[16], t_input *hm,
			uint64_t hex_3[4])
{
	t_list *head;

	head = NULL;
	form_keys3(keys, hex_3, 2);
	decrypt_des3(hm, options, keys, &head);
	options.a = 0;
	free(hm->s);
	hm->size = (size_t)u_allocate(head, &(hm->s));
	ft_lst_clear(&head);
	form_keys3(keys, hex_3, 1);
	g_ultra_flag = 1;
	encrypt_des3(hm, keys, &head);
	free(hm->s);
	hm->size = (size_t)u_allocate(head, &(hm->s));
	ft_lst_clear(&head);
	form_keys3(keys, hex_3, 0);
	decrypt_des3(hm, options, keys, &head);
	g_ultra_flag = 2;
	output_des3(&head, options, hm);
}

void	encrypt_des3(t_input *inp, uint64_t keys[16], t_list **head)
{
	size_t	pos;
	uint8_t	tmp[9];
	uint8_t	*hm;

	pos = 0;
	checking_upon_null_input_dc(inp, keys, head, &hm);
	while (inp->size > 0)
	{
		free(hm);
		if (inp->size == 8)
		{
			fill_w_pad(tmp, 0, inp, &pos);
			if (!g_ultra_flag)
			{
				free(hm);
				fill_w_pad(tmp, 8, inp, &pos);
				ft_lst_push_back(head, (hm = encrypt_d(keys, tmp)), 8);
			}
		}
		else
			fill_w_pad(tmp, inp->size > 8 ? 0 : 8 - inp->size, inp, &pos);
		ft_lst_push_back(head, (hm = encrypt_d(keys, tmp)), 8);
	}
	free(hm);
}

void	output_des3(t_list **head, t_options options, t_input *inp)
{
	uint8_t	*rez;
	ssize_t	count;
	char	base[65];

	count = u_allocate(*head, &rez);
	if (options.d && g_ultra_flag == 2)
		make_it_butiful_decryption(&rez, &count);
	if (options.a && !options.d)
	{
		initialize_base(base);
		ft_strdel((char **)&(inp->s));
		inp->s = rez;
		inp->size = count;
		encrypt_base64(inp, options, base);
	}
	else
	{
		write(options.out, rez, (size_t)count);
		free(rez);
	}
	ft_lst_clear(head);
}
