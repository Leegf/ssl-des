/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 16:09:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 15:10:29 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void	initialize_base(char *tmp)
{
	int		i;
	char	let;

	i = 0;
	let = 'A';
	while (i < 26)
		tmp[i++] = let++;
	let = 'a';
	while (i < 52)
		tmp[i++] = let++;
	let = '0';
	while (i < 62)
		tmp[i++] = let++;
	tmp[i++] = '+';
	tmp[i++] = '/';
	tmp[i] = '\0';
}

void	process_to_ciph(t_options options, char *base)
{
	t_input *hm;

	if (!(hm = read_input(options.in)))
		exit(1);
	if (options.d)
		decrypt_base64(hm, options, base);
	else
		encrypt_base64(hm, options, base);
}

int		make_base64(char **av, int ac)
{
	char		base[65];
	t_options	options;

	initialize_base(base);
	initialize_options(&options);
	read_options(av, &options, ac);
	process_to_ciph(options, base);
	return (0);
}
