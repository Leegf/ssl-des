/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_options.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 15:32:50 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/06 13:21:46 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void		prepare_the_key(t_options *options)
{
	uint8_t		*rez_of_rez;
	int			count;

	rez_of_rez = ft_strnew_u(16);
	count = 0;
	if (!ft_is_hex(options->hex))
		error_hex("hex");
	while (42)
	{
		while ((size_t)count < ft_strlenuu(options->hex) && count < 16)
		{
			rez_of_rez[count] = options->hex[count];
			count++;
		}
		if (count >= 16)
			break ;
		rez_of_rez[count++] = 0;
	}
	options->hex = NULL;
	options->hex = rez_of_rez;
}

void		check_options_des(t_options *options)
{
	if (options->hex)
		prepare_the_key(options);
	else
	{
		options->hex = (uint8_t *)getpass("Enter key in the hex: ");
		prepare_the_key(options);
	}
}

void		checking_upon_null_input(t_input *inp,
									uint64_t keys[16], t_list **head)
{
	size_t		pos;
	uint8_t		tmp[9];

	pos = 0;
	if (head && inp->size == 0)
	{
		fill_w_pad(tmp, 8, inp, &pos);
		ft_lst_push_back(head, encrypt_d(keys, tmp), 8);
	}
}

void		output_des(t_list **head, t_options options, t_input *inp)
{
	uint8_t		*rez;
	ssize_t		count;
	char		base[65];

	count = u_allocate(*head, &rez);
	if (options.d)
		make_it_butiful_decryption(&rez, &count);
	if (options.a && !options.d)
	{
		initialize_base(base);
		ft_strdel((char **)&(inp->s));
		inp->s = rez;
		inp->size = count;
		encrypt_base64(inp, options, base);
	}
	else
	{
		write(options.out, rez, (size_t)count);
		free(rez);
	}
	ft_lst_clear(head);
}
