/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des3-2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 19:19:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/06 13:15:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void		initialize_few_variables(int *count, int *count2)
{
	*count = 0;
	*count2 = 0;
}

void		prepare_the_key3(t_options *options, uint64_t hex_3[4], int index,
							size_t len)
{
	uint8_t		*rez_of_rez;
	int			count;
	int			count2;

	if (index > 2)
		return ;
	rez_of_rez = ft_strnew_u(16);
	initialize_few_variables(&count, &count2);
	while (42)
	{
		while (count < (int)len && count < 16)
		{
			rez_of_rez[count] = options->hex[count];
			count++;
			count2 = count;
		}
		if (count >= 16)
			break ;
		rez_of_rez[count++] = 0;
	}
	len -= count2;
	hex_3[index] = ft_atoh(rez_of_rez);
	ft_strdel((char **)&rez_of_rez);
	options->hex += count2;
	prepare_the_key3(options, hex_3, index + 1, len);
}

void		check_options_des3(t_options *options, uint64_t hex_3[4])
{
	size_t hm;

	hm = ft_strlenuu(options->hex);
	if (options->hex)
	{
		if (!ft_is_hex(options->hex))
			error_hex("hex");
		prepare_the_key3(options, hex_3, 0, hm);
	}
	else
	{
		options->hex = (uint8_t *)getpass("Enter key in the hex: ");
		if (!ft_is_hex(options->hex))
			error_hex("hex");
		prepare_the_key3(options, hex_3, 0, hm);
	}
}

void		form_keys3(uint64_t keys[16], uint64_t hex_3[4], int i)
{
	uint64_t	rez;
	size_t		count;

	count = 0;
	while (count < 16)
		keys[count++] = 0;
	rez = hex_3[i];
	first_permutation_of_key(&rez);
	creating_key_pairs(rez, keys);
	keys_permutation(keys);
}

void		checking_upon_null_input_dc(t_input *inp,
					uint64_t keys[16], t_list **head, uint8_t **rez)
{
	size_t		pos;
	uint8_t		tmp[9];

	pos = 0;
	*rez = NULL;
	if (head && inp->size == 0)
	{
		fill_w_pad(tmp, 8, inp, &pos);
		ft_lst_push_back(head, encrypt_d(keys, tmp), 8);
	}
}
