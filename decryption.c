/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decryption.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 16:06:46 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/06 13:15:22 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

t_out		decrypt_base64_m(t_input *inp, char *base)
{
	size_t			count;
	uint32_t		ui_buf;
	size_t			k;
	t_out			out;

	count = 0;
	k = 0;
	out.p = buf_o_d(*inp, &(out.buf));
	while (count < inp->size - 1)
	{
		ui_buf = 0;
		while (inp->s[count] == '\n' || inp->s[count] == '\t'
			|| inp->s[count] == ' ')
			count++;
		make_int_from_sixlets(&count, &ui_buf, *inp, base);
		out.buf[k++] = ((255 << 16) & ui_buf) >> 16;
		out.buf[k++] = ((255 << 8) & ui_buf) >> 8;
		out.buf[k++] = 255 & ui_buf;
	}
	return (out);
}

uint8_t		*decrypt_d(uint64_t keys[16], uint8_t tmp[9])
{
	int			count;
	uint64_t	rez;
	uint64_t	pairs[17][2];

	rez = 0;
	initialize_rez(&rez, tmp);
	count = 1;
	first_permutation_of_data(&rez);
	pairs[0][0] = (rez & (MASK_D << 32)) >> 32;
	pairs[0][1] = rez & MASK_D;
	while (count <= 16)
	{
		pairs[count][0] = pairs[count - 1][1];
		pairs[count][1] = pairs[count - 1][0] ^ function_f(keys[15 - (count
												- 1)], pairs[count - 1][1]);
		count++;
	}
	rez = (pairs[16][1] << 32) | pairs[16][0];
	finelest_permutation(&rez);
	return (transform_to_uint_8(rez));
}

void		fill_tmp(uint8_t tmp[9], t_input *inp, size_t *pos)
{
	size_t count;
	size_t count2;
	size_t diff;

	count = 0;
	count2 = 0;
	diff = 0;
	if (inp->size < 8)
		diff = 8 - inp->size;
	while (count < inp->size && count < 8)
		tmp[count++] = inp->s[(*pos)++];
	if (inp->size >= 8)
		inp->size -= 8;
	else
		inp->size -= inp->size;
	while (count2 < diff)
	{
		tmp[count++] = '\0';
		count2++;
	}
}

void		check_whether_base64_is_on(t_input *inp, t_options options)
{
	char	base[65];
	t_out	hm;

	if (!options.a)
		return ;
	initialize_base(base);
	hm = decrypt_base64_m(inp, base);
	free(inp->s);
	inp->s = hm.buf;
	inp->size = hm.p;
}

void		decrypt_des(t_input *inp, t_options options, uint64_t keys[16],
						t_list **head)
{
	uint8_t		tmp[9];
	uint8_t		*hm;
	size_t		pos;
	size_t		rem;

	pos = 0;
	hm = NULL;
	check_whether_base64_is_on(inp, options);
	rem = inp->size;
	while (inp->size > 0)
	{
		free(hm);
		fill_tmp(tmp, inp, &pos);
		ft_lst_push_back(head, (hm = decrypt_d(keys, tmp)), 8);
		if (rem - pos < 8)
			break ;
	}
	free(hm);
	output_des(head, options, inp);
}
