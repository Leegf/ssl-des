/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64_3.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 19:50:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/14 19:50:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void	three_letters_left_cas(uint32_t ui_buf, char *base, uint8_t *out_buf,
							ssize_t k)
{
	out_buf[k++] = base[(ui_buf & MASK_64_1) >> 18];
	out_buf[k++] = base[(ui_buf & MASK_64_2) >> 12];
	out_buf[k++] = '=';
	out_buf[k] = '=';
}

void	two_letters_left_case(uint32_t ui_buf, char *base, uint8_t *out_buf,
							ssize_t k)
{
	out_buf[k++] = base[(ui_buf & MASK_64_1) >> 18];
	out_buf[k++] = base[(ui_buf & MASK_64_2) >> 12];
	out_buf[k++] = base[(ui_buf & MASK_64_3) >> 6];
	out_buf[k] = '=';
}

void	general_case(uint32_t ui_buf, char *base, uint8_t *out_buf, ssize_t *k)
{
	out_buf[(*k)++] = base[(ui_buf & MASK_64_1) >> 18];
	out_buf[(*k)++] = base[(ui_buf & MASK_64_2) >> 12];
	out_buf[(*k)++] = base[(ui_buf & MASK_64_3) >> 6];
	out_buf[(*k)++] = base[(ui_buf & MASK_64_4)];
}

void	make_int_from_octets(size_t *count, unsigned int *ui_buf, t_input inp)
{
	int j;

	j = 0;
	while (j < 3)
	{
		(*ui_buf) |= inp.s[(*count)++];
		(*ui_buf) <<= 8;
		j++;
	}
	(*ui_buf) >>= 8;
}

void	initialize_vrs_ffs_norm(ssize_t *k, size_t *count, size_t *hm)
{
	*k = 0;
	*count = 0;
	*hm = 1;
}
