/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_funcs2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 15:22:36 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 15:22:37 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void	initialize_rez(uint64_t *rez, uint8_t tmp[9])
{
	int count;

	count = 0;
	while (count < 8)
	{
		*rez |= (uint64_t)tmp[count] << (8 * (8 - (count + 1)));
		count++;
	}
}

uint8_t	*ft_strdupu_v(uint8_t *s1)
{
	size_t	i;
	uint8_t	*cp;
	size_t	len;

	len = ft_strlenuu(s1);
	i = 0;
	cp = ft_strnew_u(16);
	if (cp == NULL)
		return (NULL);
	while (i < len)
	{
		cp[i] = s1[i];
		i++;
	}
	while (i < 16)
		cp[i++] = 0;
	return (cp);
}
