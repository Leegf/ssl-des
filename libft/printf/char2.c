/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   char2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 20:18:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/17 20:18:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** 2_byte case of scenario.
*/

void	two_byte_case(unsigned int c, t_list **head)
{
	unsigned int rez;
	unsigned int a;
	unsigned int b;

	a = (c << 26) >> 26;
	b = ((c >> 6) << 27) >> 27;
	rez = (MASK_1 >> 8) | b;
	form_da_c_for_lst((char)rez, head);
	rez = (((unsigned int)MASK_1 << 24) >> 24) | a;
	form_da_c_for_lst((char)rez, head);
}

/*
** 3_bytes case of scenario.
*/

void	three_byte_case(unsigned int c, t_list **head)
{
	unsigned int rez;
	unsigned int a;
	unsigned int b;
	unsigned int d;

	a = (c << 26) >> 26;
	b = ((c >> 6) << 26) >> 26;
	d = ((c >> 12) << 28) >> 28;
	rez = (MASK_2 >> 16) | d;
	form_da_c_for_lst((char)rez, head);
	rez = (((unsigned int)MASK_2 << 16) >> 24) | b;
	form_da_c_for_lst((char)rez, head);
	rez = (((unsigned int)MASK_2 << 24) >> 24) | a;
	form_da_c_for_lst((char)rez, head);
}

/*
** 4_bytes case of scenario.
*/

void	four_byte_case(unsigned int c, t_list **head)
{
	long			rez;
	unsigned int	a;
	unsigned int	b;
	unsigned int	d;
	unsigned int	e;

	a = (c << 26) >> 26;
	b = ((c >> 6) << 26) >> 26;
	d = ((c >> 12) << 26) >> 26;
	e = ((c >> 18) << 29) >> 29;
	rez = (MASK_3 >> 24) | e;
	form_da_c_for_lst((char)rez, head);
	rez = (MASK_3 << 8) >> 24 | d;
	form_da_c_for_lst((char)rez, head);
	rez = ((MASK_3 << 16) >> 24) | b;
	form_da_c_for_lst((char)rez, head);
	rez = ((MASK_3 << 24) >> 24) | a;
	form_da_c_for_lst((char)rez, head);
}

/*
** Additional func which will help a lot. I mean, which makes decision upon how
** many bytes our w_char takes.
*/

void	parse_uc(int size, unsigned int c, t_list **head)
{
	if (size <= 7 || MB_CUR_MAX == 1)
		form_da_c_for_lst((char)c, head);
	else if (size <= 11)
		two_byte_case(c, head);
	else if (size <= 16)
		three_byte_case(c, head);
	else
		four_byte_case(c, head);
}

/*
** The wonderful thing, which will make Unicode work. (I hope so very much.)
*/

void	write_l_char(t_flags *flags, unsigned int c, t_list **head)
{
	unsigned int size;

	size = count_those_great_bits(c);
	if (MB_CUR_MAX > 1 && (size > 7 && size <= 11))
		(*flags).width -= 1;
	else if (MB_CUR_MAX > 1 && size > 11 && size < 17)
		(*flags).width -= 2;
	else if (MB_CUR_MAX > 1 && size >= 17)
		(*flags).width -= 3;
	if ((*flags).minus)
	{
		parse_uc(size, c, head);
		form_width((*flags), head);
	}
	else
	{
		form_width((*flags), head);
		parse_uc(size, c, head);
	}
}
