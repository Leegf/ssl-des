/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 15:19:47 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/04 15:19:48 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Checking whether c is one of the conversion' flags
*/

int		check_conv(char c)
{
	if (c == 's' || c == 'S' || c == 'p' || c == 'd' || c == 'D' || c == 'i'
		|| c == 'o' || c == 'O' || c == 'u' || c == 'U' || c == 'x' ||
		c == 'X' || c == 'c' || c == 'C' || c == '%' || c == 'n' || c == 'b')
		return (1);
	return (0);
}

/*
** none = 0
** hh = 1
** h = 2
** ll = 3
** l = 4
** j = 5
** z = 6
*/

void	parse_length_n_conversion(t_flags *flags, char **fmt)
{
	while ((**fmt) == 'h' || (**fmt) == 'l' || (**fmt) == 'z' || (**fmt) == 'j')
	{
		parse_length_add(flags, fmt);
		if (**fmt == 'j')
			(*flags).length = 5;
		else if (**fmt == 'z')
			(*flags).length = 6;
		(*fmt)++;
	}
	if (check_conv(**fmt))
	{
		(*flags).conv = **fmt;
		(*fmt)++;
	}
	else
	{
		if (**fmt == '\0')
		{
			(*flags).conv = 0;
			return ;
		}
		(*flags).conv = **fmt;
		(*fmt)++;
	}
}

/*
** Calls functions which are needed for appropriate flags.
** Deals with cCsSdDi.
*/

void	f_p_parsing(va_list ax, t_flags *flags, t_list **head,
					const t_wrti arr[4])
{
	int i;

	i = 0;
	while (i < 3)
	{
		if (ft_strchr(arr[i].s, (*flags).conv))
		{
			if ((*flags).conv == 'c' || (*flags).conv == 'C')
				arr[i].wr_i(&(*flags), va_arg(ax, int), head);
			else if ((*flags).conv == 's' || (*flags).conv == 'S')
				arr[i].wr_p(&(*flags), va_arg(ax, void *), head);
			else if ((*flags).conv == 'd' || (*flags).conv == 'D'
					|| (*flags).conv == 'i')
			{
				if ((*flags).length >= 3 || (*flags).conv == 'D')
					arr[i].wr_p(&(*flags), va_arg(ax, void *), head);
				else
					arr[i].wr_i(&(*flags), va_arg(ax, int), head);
			}
			else
				arr[i].wr_p(flags, va_arg(ax, void *), head);
		}
		i++;
	}
}

/*
** Calls functions which are needed for appropriate flags.
** Deals with uUoOxXp.
*/

void	s_p_parsing(va_list ax, t_flags *flags, t_list **head,
					const t_wrtl arr2[3])
{
	int i;

	i = 0;
	while (i < 3)
	{
		if (ft_strchr(arr2[i].s, (*flags).conv))
		{
			if ((*flags).conv == 'p')
			{
				(*flags).hash = 1;
				(*flags).length = 4;
				write_x_l(&(*flags), va_arg(ax, void *), head);
			}
			else if ((*flags).length >= 3 || (*flags).conv == 'U'
					|| (*flags).conv == 'O' || (*flags).conv == 'b')
				arr2[i].wr_p(&(*flags), va_arg(ax, void *), head);
			else
				arr2[i].wr_i(&(*flags), va_arg(ax, unsigned int), head);
		}
		i++;
	}
}

void	parse_prec(t_flags *flags, char **fmt, va_list ax)
{
	int		i;

	i = 0;
	if (**fmt == '.')
	{
		(*flags).point = 1;
		*fmt += 1;
		while ((ft_isdigit((*fmt)[i])) || ((*fmt)[i] == '-'))
			i++;
		(*flags).prec = ft_atoi(*fmt);
		if ((*flags).prec <= -1)
			(*flags).prec = (-1);
		*fmt += i;
	}
	if (i > 0 && **fmt == '.')
	{
		*fmt += 1;
		parse_star_p(fmt, flags, ax);
	}
	if (**fmt == '*')
		parse_star_p(fmt, flags, ax);
}
