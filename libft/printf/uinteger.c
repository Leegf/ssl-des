/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   uinteger.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 15:13:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/29 12:06:37 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	cast_it_u(unsigned int *c, unsigned char *d, t_flags flags)
{
	if (c == NULL)
	{
		*d = (flags.length) == 1 ? (unsigned char)*d : *d;
		*d = (flags.length) == 2 ? (unsigned short)*d : *d;
		*d = (flags.length) == 3 ? (unsigned long long)*d : *d;
		*d = (flags.length) == 4 ? (unsigned long)*d : *d;
		*d = (flags.length) == 5 ? (uintmax_t) * d : *d;
		*d = (flags.length) == 6 ? (size_t)*d : *d;
	}
	if (d == NULL)
	{
		*c = (flags.length) == 1 ? (unsigned char)*c : *c;
		*c = (flags.length) == 2 ? (unsigned short)*c : *c;
		*c = (flags.length) == 3 ? (unsigned long long)*c : *c;
		*c = (flags.length) == 4 ? (unsigned long)*c : *c;
		*c = (flags.length) == 5 ? (uintmax_t) * c : *c;
		*c = (flags.length) == 6 ? (size_t)*c : *c;
	}
}

void	write_u_i(t_flags *flags, unsigned int c, t_list **head)
{
	int		size_of_int;
	char	*str;

	cast_it_u(&c, NULL, *flags);
	str = ft_u_itoa_base(c, 10, 0);
	size_of_int = str[0] == '-' ? ft_strlen(str) - 1 : ft_strlen(str);
	if (((*flags).width && (*flags).prec == -1) || (!(*flags).width
													&& (*flags).prec == -1))
		make_int(flags, str, head, size_of_int);
	else if (!((*flags).width) && (*flags).prec > 0)
		make_int(flags, str, head, size_of_int);
	else
		make_int(flags, str, head, size_of_int);
	ft_strdel(&str);
}

void	write_u_l(t_flags *flags, void *c, t_list **head)
{
	int		size_of_int;
	char	*str;

	cast_it_u(NULL, (unsigned char *)&c, *flags);
	if ((*flags).conv == 'b')
		str = ft_u_itoa_base((size_t)c, 2, 1);
	else
		str = ft_u_itoa_base((size_t)c, 10, 0);
	size_of_int = str[0] == '-' ? ft_strlen(str) - 1 : ft_strlen(str);
	if (((*flags).width && (*flags).prec == -1)
		|| (!(*flags).width && (*flags).prec == -1))
		make_int(flags, str, head, size_of_int);
	else if (!((*flags).width) && (*flags).prec > 0)
		make_int(flags, str, head, size_of_int);
	else
		make_int(flags, str, head, size_of_int);
	ft_strdel(&str);
}
