/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   integer2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */ /*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 13:17:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/28 20:32:46 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Forms a string without making width.
*/

void	form_d_for_case_2(t_flags *flags, char *n, t_list **head, int s)
{
	char	sign;
	int		num;

	sign = n[0] == '-' ? '-' : '+';
	num = count_that_shit((*flags), sign, s, n);
	if ((*flags).zero && (*flags).width > num && !(*flags).minus
		&& (*flags).prec == -1)
		make_zero_work(flags, head, '7', n);
	else if ((*flags).zero && (*flags).width > num &&
			!(*flags).minus && (*flags).prec > 0)
		make_zero_work(flags, head, sign, n);
	else
		make_zero_work(flags, head, sign, n);
	if ((*flags).prec - s > 0)
	{
		if (((*flags).conv == 'x' || (*flags).conv == 'X'
			|| (*flags).conv == 'p') && !fk_p((*flags), sign))
			hex_fix(flags, head, n);
		stuff_it((*flags).prec - s, head, (*flags), 0);
	}
	to_the_exit((*flags), head, n, s);
}

/*
** So... forms a string considering all flags etc.
*/

void	make_int(t_flags *flags, char *n, t_list **head, int s)
{
	if ((*flags).minus)
	{
		form_d_for_case_2(flags, n, head, s);
		form_width_c2(flags, head, s, n);
	}
	else
	{
		form_width_c2(flags, head, s, n);
		form_d_for_case_2(flags, n, head, s);
	}
}

/*
** Casts the int to the appropriate type considering
** length.
*/

void	cast_it(int *c, unsigned char *d, t_flags flags)
{
	if (c == NULL)
	{
		*d = (flags.length) == 1 ? (char)*d : *d;
		*d = (flags.length) == 2 ? (short)*d : *d;
		*d = (flags.length) == 3 ? (long long)*d : *d;
		*d = (flags.length) == 4 ? (long)*d : *d;
		*d = (flags.length) == 5 ? (intmax_t) * d : *d;
		*d = (flags.length) == 6 ? (ssize_t) * d : *d;
	}
	if (d == NULL)
	{
		*c = (flags.length) == 1 ? (char)*c : *c;
		*c = (flags.length) == 2 ? (short)*c : *c;
		*c = (flags.length) == 3 ? (long long)*c : *c;
		*c = (flags.length) == 4 ? (long)*c : *c;
		*c = (flags.length) == 5 ? (intmax_t) * c : *c;
		*c = (flags.length) == 6 ? (ssize_t) * c : *c;
	}
}

/*
** Writes an int in case of 4-bytes or lower args cases.
*/

void	write_int_i(t_flags *flags, int c, t_list **head)
{
	int		size_of_int;
	char	*str;

	cast_it(&c, NULL, *flags);
	str = ft_itoa_base(c, 10);
	size_of_int = str[0] == '-' ? ft_strlen(str) - 1 : ft_strlen(str);
	make_int(flags, str, head, size_of_int);
	ft_strdel(&str);
}

/*
** Writes an int in case of 8-bytes args.
*/

void	write_int_l(t_flags *flags, void *c, t_list **head)
{
	int		size_of_int;
	char	*str;

	cast_it(NULL, (unsigned char *)&c, *flags);
	if ((long long)c < -9223372036854775807)
		str = ft_strdup("-9223372036854775808");
	else if ((*flags).conv == 'b')
		str = ft_itoa_base((long long)c, 2);
	else
		str = ft_itoa_base((long long)c, 10);
	size_of_int = str[0] == '-' ? ft_strlen(str) - 1 : ft_strlen(str);
	make_int(flags, str, head, size_of_int);
	ft_strdel(&str);
}
