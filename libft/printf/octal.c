/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   octal.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:23:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/27 17:23:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	write_o_i(t_flags *flags, unsigned int c, t_list **head)
{
	int		size_of_int;
	char	*str;

	if (((*flags).length == 1 || (*flags).length == 2) &&
		((ssize_t)c == 2147483648 || (ssize_t)c == -2147483648))
		(*flags).point = 0;
	else
		(*flags).point = 1;
	cast_it_u(&c, NULL, *flags);
	str = ft_u_itoa_base(c, 8, 0);
	size_of_int = str[0] == '-' ? ft_strlen(str) - 1 : ft_strlen(str);
	make_int(flags, str, head, size_of_int);
	ft_strdel(&str);
}

void	write_o_l(t_flags *flags, void *c, t_list **head)
{
	int		size_of_int;
	char	*str;

	cast_it_u(NULL, (unsigned char *)&c, *flags);
	str = ft_u_itoa_base((size_t)c, 8, 0);
	size_of_int = str[0] == '-' ? ft_strlen(str) - 1 : ft_strlen(str);
	make_int(flags, str, head, size_of_int);
	ft_strdel(&str);
}

void	write_x_i(t_flags *flags, unsigned int c, t_list **head)
{
	int		size_of_int;
	char	*str;

	if ((*flags).hash && (*flags).width > (*flags).prec &&
		((*flags).prec == 0 || ((*flags).prec == -1 && (*flags).point)
		|| (*flags).prec == 1)
		&& ((*flags).conv == 'x' || (*flags).conv == 'X'))
	{
		if ((*flags).minus)
			(*flags).space = 0;
		else
			(*flags).zero = 0;
	}
	cast_it_u(&c, NULL, *flags);
	if ((*flags).conv == 'X')
		str = ft_u_itoa_base((size_t)c, 16, 1);
	else
		str = ft_u_itoa_base((size_t)c, 16, 0);
	size_of_int = str[0] == '-' ? ft_strlen(str) - 1 : ft_strlen(str);
	make_int(flags, str, head, size_of_int);
	ft_strdel(&str);
}

void	write_x_l(t_flags *flags, void *c, t_list **head)
{
	int		size_of_int;
	char	*str;

	if ((*flags).hash && (*flags).width > (*flags).prec &&
		((*flags).prec == 0 || ((*flags).prec == -1 && (*flags).point)
		|| (*flags).prec == 1) && ((*flags).conv == 'x'
									|| (*flags).conv == 'X'))
	{
		if ((*flags).minus)
			(*flags).space = 0;
		else
			(*flags).zero = 0;
	}
	cast_it_u(NULL, (unsigned char *)&c, *flags);
	if ((*flags).conv == 'X')
		str = ft_u_itoa_base((size_t)c, 16, 1);
	else
		str = ft_u_itoa_base((size_t)c, 16, 0);
	size_of_int = str[0] == '-' ? ft_strlen(str) - 1 : ft_strlen(str);
	make_int(flags, str, head, size_of_int);
	ft_strdel(&str);
}

void	hex_fix(t_flags *flags, t_list **head, char *n)
{
	if ((*flags).sign == 1)
		return ;
	if ((*flags).conv == 'U' || (*flags).conv == 'u')
		return ;
	if ((((*flags).conv == 'x' || (*flags).conv == 'X')
		&& ((*flags).hash && n[0] != '0')) || ((*flags).conv == 'p'))
	{
		if ((*flags).conv == 'x' || (*flags).conv == 'p')
			ft_lst_push_back(head, "0x", 2);
		else
			ft_lst_push_back(head, "0X", 2);
		(*flags).sign = 1;
	}
}
