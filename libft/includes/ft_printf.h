/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/06 20:41:31 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/04 17:25:09 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"
# include <stdarg.h>
# include <stdint.h>

# define MASK_1 49280
# define MASK_2 14712960
# define MASK_3 4034953344

/*
** Colors' stuff.
*/

# define DEFAULT "\e[39m"
# define BK "\e[30m"
# define RED "\e[31m"
# define GR "\e[32m"
# define YW "\e[33m"
# define BL "\e[34m"
# define MG "\e[35m"
# define CN "\e[36m"
# define LGR "\e[37m"
# define DG "\e[90m"
# define LR "\e[91m"
# define LG "\e[92m"
# define LY "\e[93m"
# define LB "\e[94m"
# define LM "\e[95m"
# define LC "\e[96m"
# define WHT "\e[97m"
# define NM "\e[0m"

typedef	struct	s_flags
{
	int	hash;
	int	zero;
	int	minus;
	int	plus;
	int	space;
	int	width;
	int	prec;
	int	length;
	int conv;
	int point;
	int perc;
	int sign;
}				t_flags;

typedef	struct	s_wrtl
{
	char *s;
	void (*wr_i)(t_flags *flags, unsigned int, t_list **head);
	void (*wr_p)(t_flags *flags, void *, t_list **head);
}				t_wrtl;

typedef	struct	s_wrti
{
	char *s;
	void (*wr_i)(t_flags *flags, int, t_list **head);
	void (*wr_p)(t_flags *flags, void *, t_list **head);
}				t_wrti;

typedef struct	s_colors
{
	char *s1;
	char *s2;
}				t_colors;

int				ft_printf(const char *format, ...);
int				parse_simple_str(char **fmt, t_list **head, t_flags *flags);
void			initialise_flags(t_flags *flags);
void			parse_flags(t_flags *flags, char **fmt);
void			parse_width_n_prec(t_flags *flags, char **fmt, va_list ax);
void			parse_length_add(t_flags *flags, char **fmt);
void			parse_length_n_conversion(t_flags *flags, char **fmt);
int				count_those_great_bits(unsigned int c);
void			form_width(t_flags flags, t_list **head);
void			write_l_char(t_flags *flags, unsigned int c, t_list **head);
void			write_char(t_flags *flags, int c, t_list **head);
void			write_string(t_flags *flags, void *st, t_list **head);
void			form_da_c_for_lst(char c, t_list **head);
void			write_l_string(t_flags *flags, wchar_t *st, t_list **head,
				int s);
void			form_width_s(t_flags flags, t_list **head, int size_of_str);
void			parse_uc(int size, unsigned int c, t_list **head);
void			correct_width(t_flags *flags, wchar_t *st);
int				num_of_bytes(unsigned int size);
int				count_all_bytes(wchar_t *s);
void			write_int_i(t_flags *flags, int c, t_list **head);
void			write_int_l(t_flags *flags, void *c, t_list **head);
char			*ft_itoa_base(long n, int b);
void			to_the_exit(t_flags flags, t_list **head, char *n, int s);
void			form_width_c2(t_flags *flags, t_list **head, int s, char *n);
void			make_zero_work(t_flags *flags, t_list **head, char sign,
				char *n);
int				count_that_shit(t_flags flags, char sign, int s, char *n);
void			stuff_it(int s, t_list **head, t_flags flags, int ultra_f);
void			make_int(t_flags *flags, char *n, t_list **head, int s);
char			*ft_u_itoa_base(size_t n, int b, int flag);
void			parse_prec(t_flags *flags, char **fmt, va_list ax);

void			write_u_i(t_flags *flags, unsigned int c, t_list **head);
void			write_u_l(t_flags *flags, void *c, t_list **head);
void			cast_it_u(unsigned int *c, unsigned char *d, t_flags flags);

void			write_o_i(t_flags *flags, unsigned int c, t_list **head);
void			write_o_l(t_flags *flags, void *c, t_list **head);

void			write_x_i(t_flags *flags, unsigned int c, t_list **head);
void			write_x_l(t_flags *flags, void *c, t_list **head);

void			hex_fix(t_flags *flags, t_list **head, char *n);

void			f_p_parsing(va_list ax, t_flags *flags, t_list **head,
					const t_wrti arr[3]);
void			s_p_parsing(va_list ax, t_flags *flags, t_list **head,
					const t_wrtl arr2[3]);

void			to_exit_2(t_flags flags, t_list **head, char *n, int s);
int				check_conv(char c);
int				fk_p(t_flags flags, char sign);
int				check_flags(t_flags flags);
int				count_that_shit2(t_flags flags, char sign, int s, char *n);

ssize_t			parsing_d(int fd, va_list ax, char *fmt, t_list **head);
int				ft_dprintf(int fd, const char *format, ...);
void			writing_variadic_args(va_list ax, t_flags flags, t_list **head);
ssize_t			allocate(t_list *tmp, char **line);
void			write_n(t_flags *flags, void *c, t_list **head);
void			parse_colors(char **fmt, t_list **head, t_flags *flags);
void			parse_star_w(char **fmt, t_flags *flags, va_list ax);
void			parse_star_p(char **fmt, t_flags *flags, va_list ax);

int				costul1(t_flags *flags, t_list **head, char *n);
void			costul2(t_flags *flags, t_list **head, char *n);
int				costul3(t_flags *flags, t_list **head, char sign);

#endif
