/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 19:38:11 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:05:21 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Allocate (with malloc(3)) and returns a “fresh” string end- ing with ’\0’
** representing the integer n given as argument. Negative numbers must
** be supported. If the allocation fails, the function returns NULL.
*/

#include "libft.h"

static	int		allocate_space(long n, char **str, int sign, size_t *tens)
{
	size_t count;

	count = 0;
	while (n / 10 >= 1)
	{
		*tens *= 10;
		n /= 10;
		count++;
	}
	if (n == 0)
		*str = ft_strnew(1);
	else
		*str = ft_strnew(count + sign + 1);
	if (*str == NULL)
		return (1);
	return (0);
}

char			*ft_itoa(int n)
{
	size_t			i;
	char			*res;
	int				sign;
	long			n_cp;
	size_t			tens;

	n_cp = n;
	sign = n < 0 ? 1 : 0;
	tens = 1;
	i = 0;
	if (sign == 1)
		n_cp *= -1;
	if (allocate_space(n_cp, &res, sign, &tens))
		return (NULL);
	res[sign == 1 ? i++ : i] = '-';
	while (tens >= 1)
	{
		res[i++] = n_cp / tens + '0';
		n_cp %= tens;
		tens /= 10;
	}
	return (res);
}
