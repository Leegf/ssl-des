/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   encryption.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 15:36:06 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/06 13:22:12 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

/*
** Function which fills block of 64 with whatever it needs to be filled with.
*/

void		fill_w_pad(uint8_t tmp[9], int diff, t_input *inp, size_t *pos)
{
	size_t		count;
	size_t		count2;

	count = 0;
	count2 = 0;
	while (count < inp->size && count < 8)
		tmp[count++] = inp->s[(*pos)++];
	while (count2 < (size_t)diff)
	{
		tmp[count++] = diff;
		count2++;
	}
	tmp[count] = '\0';
	if (!diff)
		inp->size -= 8;
	else
		inp->size -= inp->size;
}

void		encrypt_des(t_input *inp, t_options options, uint64_t keys[16],
						t_list **head)
{
	size_t		pos;
	uint8_t		tmp[9];
	uint8_t		*hm;

	pos = 0;
	hm = NULL;
	checking_upon_null_input(inp, keys, head);
	while (inp->size > 0)
	{
		free(hm);
		if (inp->size > 8)
			fill_w_pad(tmp, 0, inp, &pos);
		else if (inp->size < 8)
			fill_w_pad(tmp, 8 - inp->size, inp, &pos);
		else
		{
			i_luv_norm(inp, keys, head, &pos);
			hm = NULL;
			break ;
		}
		ft_lst_push_back(head, (hm = encrypt_d(keys, tmp)), 8);
	}
	free(hm);
	output_des(head, options, inp);
}

uint8_t		*encrypt_d(uint64_t keys[16], uint8_t tmp[9])
{
	int			count;
	uint64_t	rez;
	uint64_t	pairs[17][2];

	rez = 0;
	initialize_rez(&rez, tmp);
	count = 1;
	first_permutation_of_data(&rez);
	pairs[0][0] = (rez & (MASK_D << 32)) >> 32;
	pairs[0][1] = rez & MASK_D;
	while (count <= 16)
	{
		pairs[count][0] = pairs[count - 1][1];
		pairs[count][1] = pairs[count - 1][0] ^ function_f(keys[count - 1],
														pairs[count - 1][1]);
		count++;
	}
	rez = (pairs[16][1] << 32) | pairs[16][0];
	finelest_permutation(&rez);
	return (transform_to_uint_8(rez));
}

uint8_t		*transform_to_uint_8(uint64_t rez)
{
	int			count;
	uint8_t		*out;

	count = 0;
	out = ft_strnew_u(8);
	while (count < 8)
	{
		count++;
		out[8 - count] = ((MASK_8_B << ((count - 1) * 8)) & rez) >> ((count - 1)
																	* 8);
	}
	return (out);
}

uint64_t	function_f(uint64_t key, uint64_t r)
{
	int			count;
	uint64_t	rez;

	count = 0;
	rez = 0;
	permutate_r(&r);
	r ^= key;
	while (count < 8)
	{
		rez |= (sbox_it((MASK_6_B << ((8 - (count + 1)) * 6) & r) >> ((8 -
				(count + 1)) * 6), count)) << ((8 - (count + 1)) * 4);
		count++;
	}
	permutation_after_sboxes(&rez);
	return (rez);
}
