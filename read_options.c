/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_options.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 19:42:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/14 19:42:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

t_input		*read_input(int fd)
{
	uint8_t		buf[IN_SIZE + 1];
	int			ret;
	t_list		*head;
	t_input		*hm;

	hm = malloc(sizeof(t_input));
	hm->s = NULL;
	buf[IN_SIZE] = '\0';
	head = NULL;
	while ((ret = read(fd, buf, IN_SIZE)))
	{
		if (ret == -1)
			return (NULL);
		ft_lst_push_back(&head, buf, ret);
	}
	if (head)
		hm->size = u_allocate(head, &(hm->s));
	else
	{
		ft_lst_push_back(&head, "", 0);
		u_allocate(head, &(hm->s));
		hm->size = 0;
	}
	ft_lst_clear(&head);
	return (hm);
}

void		initialize_options(t_options *ops)
{
	(*ops).d = 0;
	(*ops).in = 0;
	(*ops).a = 0;
	(*ops).v = NULL;
	(*ops).hex = NULL;
	(*ops).out = 1;
	(*ops).bl = 0;
}

void		read_options(char **av, t_options *ops, int ac)
{
	int i;

	i = 2;
	while (i < ac)
	{
		if (ft_strequ(av[i], "-e"))
			(*ops).d = 0;
		else if (ft_strequ(av[i], "-d"))
			(*ops).d = 1;
		else if (ft_strequ(av[i], "-i"))
			read_fd(av, ops, ac, &i);
		else if (ft_strequ(av[i], "-o"))
			read_out(av, ops, ac, &i);
		else if (ft_strequ(av[i], "-a"))
			(*ops).a = 1;
		else if (ft_strequ(av[i], "-k"))
			read_hex(av, ops, ac, &i);
		else if (ft_strequ(av[i], "-v"))
			read_v(av, ops, ac, &i);
		else
			error_options(av[i]);
		i++;
	}
}

void		error_options(char *s)
{
	if (s != NULL)
		ft_printf("unknown option '%s'\n", s);
	ft_printf("Options are:\n");
	ft_printf("-i <file>\tinput file\n");
	ft_printf("-o <file>\toutput file\n");
	ft_printf("-e\t\tencrypt\n");
	ft_printf("-d\t\tdecrypt\n");
	ft_printf("-a\t\tbase64 encode/decode\n");
	ft_printf("-k\t\tkey in hex is the next argument\n");
	ft_printf("-v\t\tiv in hex is the next argument\n");
	ft_printf("Cipher Types\n-des\n-des-cbc\n-des-ecb\n-des3\n");
	exit(1);
}
