/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_options2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 19:46:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/14 19:46:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void	wrong_file(char *s)
{
	ft_printf("%s: No such file or directory\n", s);
	exit(2);
}

void	read_fd(char **av, t_options *ops, int ac, int *i)
{
	if ((*ops).in)
	{
		close((*ops).in);
		(*ops).in = 0;
	}
	if (*i + 1 >= ac)
		error_options(NULL);
	(*ops).in = open(av[*i + 1], O_RDONLY);
	if ((*ops).in == -1)
		wrong_file(av[*i + 1]);
	(*i)++;
}

void	read_out(char **av, t_options *ops, int ac, int *i)
{
	if (*i + 1 >= ac)
		error_options(NULL);
	(*ops).out = open(av[*i + 1], O_CREAT | O_WRONLY);
	if ((*ops).out == -1)
		exit(2);
	(*i)++;
}

void	read_hex(char **av, t_options *ops, int ac, int *i)
{
	if (*i + 1 >= ac)
		error_options(NULL);
	(*ops).hex = (uint8_t *)av[*i + 1];
	if (!ft_strequ(av[1], "base64") && !ft_is_hex((*ops).hex))
		error_hex("key");
	(*i)++;
}

void	read_v(char **av, t_options *ops, int ac, int *i)
{
	if (*i + 1 >= ac)
		error_options(NULL);
	(*ops).v = (uint8_t *)av[*i + 1];
	if (!ft_strequ(av[1], "base64") && !ft_is_hex((*ops).v))
		error_hex("key");
	(*i)++;
}
