/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des3-cbc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 19:28:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/02 19:28:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void	check_options_des_cbc3(t_options *options, uint64_t hex_3[4])
{
	size_t hm;

	hm = ft_strlenuu(options->hex);
	if (options->hex)
		prepare_the_key3(options, hex_3, 0, hm);
	else
	{
		options->hex = (uint8_t *)getpass("Enter key in the hex: ");
		prepare_the_key3(options, hex_3, 0, hm);
	}
	if (options->v)
		prepare_the_v(options);
	else
	{
		options->v = (uint8_t *)getpass("Enter iv in the hex: ");
		prepare_the_v(options);
	}
}

void	process_to_ciph_d_cbc3(t_options *options, uint64_t keys[16],
							t_input *hm, uint64_t hex_3[4])
{
	if (options->d)
		des3_d_cbc(options, keys, hm, hex_3);
	else
		des3_e_cbc(options, keys, hm, hex_3);
	if (!options->a || options->d)
	{
		free(hm->s);
		free(hm);
	}
}

void	for_the_sake_of_norm(t_options *options, uint8_t *tmp)
{
	options->bl = 0;
	options->v = ft_strdupu_v(tmp);
}

int		make_des3_cbc(char **av, int ac)
{
	t_options	options;
	uint64_t	keys[16];
	uint64_t	hex_3[4];
	t_input		*inp;

	hex_3[3] = '\0';
	initialize_options(&options);
	read_options(av, &options, ac);
	check_options_des_cbc3(&options, hex_3);
	if (!(inp = read_input(options.in)))
		exit(1);
	form_keys3(keys, hex_3, 0);
	process_to_ciph_d_cbc3(&options, keys, inp, hex_3);
	return (0);
}
