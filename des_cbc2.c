/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des3-cbc2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 16:56:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/06 13:21:25 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void		prepare_the_v(t_options *options)
{
	uint8_t		*rez_of_rez;
	int			count;

	rez_of_rez = ft_strnew_u(16);
	count = 0;
	if (!ft_is_hex(options->v))
		error_hex("iv");
	while (42)
	{
		while ((size_t)count < ft_strlenuu(options->v) && count < 16)
		{
			rez_of_rez[count] = options->v[count];
			count++;
		}
		if (count >= 16)
			break ;
		rez_of_rez[count++] = 0;
	}
	options->v = NULL;
	options->v = rez_of_rez;
}

uint8_t		*decrypt_d_c(uint64_t keys[16], uint8_t tmp[8], t_options *options,
							uint64_t tmp2)
{
	int			count;
	uint64_t	rez;
	uint64_t	pairs[17][2];

	rez = 0;
	initialize_rez(&rez, tmp);
	tmp2 = options->bl;
	options->bl = rez;
	count = 1;
	first_permutation_of_data(&rez);
	pairs[0][0] = (rez & (MASK_D << 32)) >> 32;
	pairs[0][1] = rez & MASK_D;
	while (count <= 16)
	{
		pairs[count][0] = pairs[count - 1][1];
		pairs[count][1] = pairs[count - 1][0] ^ function_f(keys[15 - (count -
				1)], pairs[count - 1][1]);
		count++;
	}
	rez = (pairs[16][1] << 32) | pairs[16][0];
	finelest_permutation(&rez);
	initial_vector(options, &rez, tmp2, 1);
	return (transform_to_uint_8(rez));
}

void		decrypt_des_c(t_input *inp, t_options *options, uint64_t keys[16],
						t_list **head)
{
	uint8_t		tmp[9];
	uint8_t		*hm;
	size_t		pos;
	size_t		rem;
	uint64_t	rez;

	pos = 0;
	hm = NULL;
	rez = 0;
	check_whether_base64_is_on(inp, *options);
	rem = inp->size;
	while (inp->size > 0)
	{
		free(hm);
		fill_tmp(tmp, inp, &pos);
		hm = decrypt_d_c(keys, tmp, options, rez);
		ft_lst_push_back(head, hm, 8);
		if (rem - pos < 8)
			break ;
	}
	free(hm);
}

void		check_options_des_cbc(t_options *options)
{
	if (options->hex)
		prepare_the_key(options);
	else
	{
		options->hex = (uint8_t *)getpass("Enter key in the hex: ");
		prepare_the_key(options);
	}
	if (options->v)
		prepare_the_v(options);
	else
	{
		options->v = (uint8_t *)getpass("Enter iv in the hex: ");
		prepare_the_v(options);
	}
}

void		initial_vector(t_options *options, uint64_t *rez, uint64_t tmp,
						int flag)
{
	if (options->v)
	{
		(*rez) ^= ft_atoh(options->v);
		free(options->v);
		options->v = NULL;
	}
	if (!flag)
		(*rez) ^= options->bl;
	else
		(*rez) ^= tmp;
}
