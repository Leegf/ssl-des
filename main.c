/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/08 13:42:45 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/08 13:44:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

/*
** Error output in case of wrong command.
*/

void	wrong_command_error(char *s)
{
	ft_printf("ft_ssl: Error: '%s' is an invalid command.\n\n", s);
	ft_printf("Standard commands:\n\n");
	ft_printf("Message Digest commands:\n\n");
	ft_printf("Cipher commands:\nbase64\ndes\ndes-ecb\ndes-cbc\n");
	ft_printf("des3\ndes3-ecb\ndes3-cbc\n");
	exit(1);
}

/*
** Here takes place initial checking upon what cipher or whatever to use
** further.
*/

void	cipher_parse(char **av, const t_cmnds arr_p[NUM_C], int ac)
{
	int		i;
	size_t	len;

	i = -1;
	while (++i < NUM_C)
	{
		len = ft_strlen(av[1]);
		if (len < ft_strlen(arr_p[i].s) && ft_strnequ(av[1], arr_p[i].s, len))
			wrong_command_error(av[1]);
		if (ft_strnequ(av[1], arr_p[i].s, len))
		{
			arr_p[i].ciph(av, ac);
			return ;
		}
	}
	wrong_command_error(av[1]);
}

/*
** Place, where our array of function pointers is initialized. Kinda printfy,
** wanna memorize and practice the thing more.
*/

void	cipher_parse_init(char **av, int ac)
{
	const static t_cmnds arr_p[] = {
			{"base64", &make_base64},
			{"des", &make_des_ecb},
			{"des-ecb", &make_des_ecb},
			{"des-cbc", &make_des_cbc},
			{"des3", &make_des3_ecb},
			{"des3-ecb", &make_des3_ecb},
			{"des3-cbc", &make_des3_cbc}
	};

	cipher_parse(av, arr_p, ac);
}

int		main(int ac, char **av)
{
	if (ac < 2)
	{
		ft_printf("usage: ft_ssl command [command opts] [command args]\n");
		return (1);
	}
	cipher_parse_init(av, ac);
	return (0);
}
