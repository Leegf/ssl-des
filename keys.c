/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 19:02:37 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/05 19:02:40 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

static void		join_pp_keys(uint64_t pairs[17][2], uint64_t keys[16])
{
	int count;
	int count2;

	count = 0;
	count2 = 1;
	while (count2 <= 16)
	{
		keys[count] = pairs[count2][0] << 28;
		keys[count] |= pairs[count2][1];
		count2++;
		count++;
	}
}

/*
** Divides and shifts pairs of keys.
*/

void			creating_key_pairs(uint64_t rez, uint64_t keys[16])
{
	uint64_t	pairs[17][2];
	int			count;
	int			count2;
	uint64_t	tmp;
	const int	table_shifts[] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2,
									2, 1};

	count = 0;
	tmp = 0;
	pairs[0][0] = (rez & (MASK_KEY << 28)) >> 28;
	pairs[0][1] = rez & MASK_KEY;
	while (++count <= 16)
	{
		count2 = 0;
		while (count2 < 2)
		{
			tmp = pairs[count - 1][count2] & (table_shifts[count - 1] == 1 ?
				MASK_SH1 << 27 : MASK_SH2 << 26);
			tmp >>= (table_shifts[count - 1] == 1 ? 27 : 26);
			pairs[count][count2] = ((pairs[count - 1][count2] <<
				table_shifts[count - 1]) | tmp) & MASK_KEY;
			count2++;
		}
	}
	join_pp_keys(pairs, keys);
}

/*
** It had to be placeholder for successive calling functions which in turns
** would make needed keys. It is partly so, partly not so.
*/

void			form_keys(t_options options, uint64_t keys[16])
{
	uint64_t rez;

	rez = ft_atoh(options.hex);
	if (!options.hex)
		ft_strdel((char**)&(options.hex));
	first_permutation_of_key(&rez);
	creating_key_pairs(rez, keys);
	keys_permutation(keys);
}
