/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base_64_4.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 19:52:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 15:08:48 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

size_t		buf_o_d(t_input hm, uint8_t **out)
{
	size_t			count;
	size_t			tmp;

	tmp = hm.size;
	if (hm.size / 65)
	{
		hm.size -= hm.size / 65;
		hm.size++;
	}
	count = (hm.size / 4) * 3;
	if (hm.s[tmp - 1] != '\n')
		exit(0);
	if (hm.s[tmp - 2] == '=' && hm.s[tmp - 3] == '=')
		count -= 2;
	else if (hm.s[tmp - 2] == '=')
		count -= 1;
	*out = ft_strnew_u(count);
	return (count);
}

void		make_int_from_sixlets(size_t *count, uint32_t *ui_buf, t_input inp,
								char *base)
{
	int		j;
	int		tmp;

	j = 0;
	while (j < 4)
	{
		while (inp.s[*count] == '\n' || inp.s[*count] == '\t'
			|| inp.s[*count] == ' ')
			(*count)++;
		tmp = find_pos(base, inp.s[*count]);
		if (inp.s[*count] == '=')
			tmp = 0;
		if (tmp == -1)
			exit(0);
		(*ui_buf) |= tmp;
		(*ui_buf) <<= 6;
		j++;
		(*count)++;
	}
	(*ui_buf) >>= 6;
}

void		decrypt_base64(t_input *inp, t_options options, char *base)
{
	size_t			count;
	uint32_t		ui_buf;
	size_t			k;
	t_out			out;

	count = 0;
	k = 0;
	out.p = buf_o_d(*inp, &(out.buf));
	while (count < inp->size - 1)
	{
		ui_buf = 0;
		while (inp->s[count] == '\n' || inp->s[count] == '\t'
			|| inp->s[count] == ' ')
			count++;
		make_int_from_sixlets(&count, &ui_buf, *inp, base);
		out.buf[k++] = ((255 << 16) & ui_buf) >> 16;
		out.buf[k++] = ((255 << 8) & ui_buf) >> 8;
		out.buf[k++] = 255 & ui_buf;
	}
	if (count != 0)
		free_and_write(inp, options, &out);
}

void		free_and_write(t_input *inp, t_options options, t_out *out)
{
	if (options.d)
		write(options.out, out->buf, out->p);
	else
		ft_dprintf(options.out, "%s", out->buf);
	if (!options.d)
		ft_dprintf(options.out, "\n");
	ft_strdel((char **)&(out->buf));
	free(inp->s);
	inp->s = NULL;
	free(inp);
}
