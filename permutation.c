/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   permutation.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 16:42:40 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 16:42:41 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void		first_permutation_of_key(uint64_t *rez)
{
	size_t		count;
	uint64_t	tmp;
	uint64_t	tmp2;
	uint64_t	tmp_rez;
	static int	arr[56] = {57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18,
	10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31,
	23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28,
	20, 12, 4};

	count = 0;
	tmp2 = 0;
	tmp_rez = 55;
	while (count < 56)
	{
		tmp = (*rez & ((uint64_t)1 << (64 - arr[count])));
		tmp2 |= (tmp >> (64 - arr[count])) << tmp_rez;
		count++;
		tmp_rez--;
	}
	*rez = tmp2;
}

void		permutate_key(int arr[48], uint64_t *key)
{
	size_t		count;
	uint64_t	tmp;
	uint64_t	tmp2;
	uint64_t	tmp_rez;

	count = 0;
	tmp2 = 0;
	tmp_rez = 47;
	while (count < 48)
	{
		tmp = (*key & ((uint64_t)1 << (56 - arr[count])));
		tmp2 |= (tmp >> (56 - arr[count])) << tmp_rez;
		count++;
		tmp_rez--;
	}
	*key = tmp2;
}

/*
** Makes final permutation onto joined keys.
*/

void		keys_permutation(uint64_t keys[16])
{
	int			count;
	static int	arr[48] = {14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19,
	12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45,
	33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32};

	count = 0;
	while (count < 16)
	{
		permutate_key(arr, &(keys[count]));
		count++;
	}
}

void		permutate_r(uint64_t *rez)
{
	size_t		count;
	uint64_t	tmp;
	uint64_t	tmp2;
	uint64_t	tmp_rez;
	static int	arr[48] = {32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11,
	12, 13, 12, 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24,
	25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1};

	count = 0;
	tmp2 = 0;
	tmp_rez = 47;
	while (count < 48)
	{
		tmp = (*rez & ((uint64_t)1 << (32 - arr[count])));
		tmp2 |= (tmp >> (32 - arr[count])) << tmp_rez;
		count++;
		tmp_rez--;
	}
	*rez = tmp2;
}

void		finelest_permutation(uint64_t *rez)
{
	size_t		count;
	uint64_t	tmp;
	uint64_t	tmp2;
	uint64_t	tmp_rez;
	static int	arr[64] = {40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23,
	63, 31, 38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29, 36, 4,
	44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 42, 10, 50,
	18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25};

	count = 0;
	tmp2 = 0;
	tmp_rez = 63;
	while (count < 64)
	{
		tmp = (*rez & ((uint64_t)1 << (64 - arr[count])));
		tmp2 |= (tmp >> (64 - arr[count])) << tmp_rez;
		count++;
		tmp_rez--;
	}
	*rez = tmp2;
}
