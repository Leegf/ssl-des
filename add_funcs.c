/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_funcs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 15:22:26 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 15:22:38 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

/*
** little thing which would exit from the program in a case of invalid hex input
*/

void		error_hex(const char *s)
{
	ft_printf("non-hex digit\ninvalid hex %s value\n", s);
	exit(1);
}

/*
** Simple checking upon hex's input.
*/

int			ft_is_hex(uint8_t *s)
{
	while (*s)
	{
		if ((*s >= '0' && *s <= '9') || (*s >= 'A' && *s <= 'F') ||
			(*s >= 'a' && *s <= 'f'))
			s++;
		else
			return (0);
	}
	return (1);
}

/*
** Just like ft_strlen but for unsigned.
*/

size_t		ft_strlenuu(uint8_t *s)
{
	size_t i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

uint8_t		*ft_strdupu(uint8_t *s1)
{
	size_t	i;
	uint8_t	*cp;
	size_t	len;

	len = ft_strlenuu(s1);
	i = 0;
	cp = (uint8_t *)malloc(sizeof(unsigned char) * len + 1);
	if (cp == NULL)
		return (NULL);
	while (i < len)
	{
		cp[i] = s1[i];
		i++;
	}
	cp[i] = '\0';
	return (cp);
}

/*
** Little function which will transform string into hex format.
*/

uint64_t	ft_atoh(uint8_t *s)
{
	uint64_t			rez;
	size_t				count;
	const int			tmp[6] = {10, 11, 12, 13, 14, 15};

	count = 0;
	rez = 0x0;
	while (count < 16)
	{
		if (s[count] == 0)
			rez = rez * 16 + 0;
		else if (ft_isalpha(s[count]))
		{
			if (s[count] >= 'A' && s[count] <= 'Z')
				rez = rez * 16 + tmp[s[count] - 'A'];
			else
				rez = rez * 16 + tmp[s[count] - 'a'];
		}
		else
			rez = rez * 16 + s[count] - '0';
		count++;
	}
	return (rez);
}
