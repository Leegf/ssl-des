/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 19:48:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/04 15:10:15 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ssl_des.h"

void		encrypt_base64(t_input *inp, t_options options, char *base)
{
	size_t			count;
	unsigned int	ui_buf;
	ssize_t			k;
	t_out			out;
	size_t			hm;

	initialize_vrs_ffs_norm(&k, &count, &hm);
	out.p = buf_o(inp->size, &out.buf);
	while (count < inp->size)
	{
		if (!(ui_buf = 0) && k > 0 && ((k == 64) || !((k - hm) % 64)))
		{
			out.buf[k++] = '\n';
			hm += k != 65 ? 1 : 0;
		}
		make_int_from_octets(&count, &ui_buf, *inp);
		if (count - inp->size == 1)
			two_letters_left_case(ui_buf, base, out.buf, k);
		else if (count - inp->size == 2)
			three_letters_left_cas(ui_buf, base, out.buf, k);
		else
			general_case(ui_buf, base, out.buf, &k);
	}
	if (count != 0)
		free_and_write(inp, options, &out);
}

size_t		buf_o(size_t hm, uint8_t **rez)
{
	size_t	count;

	count = hm / 3;
	count = count * 4;
	if (hm % 3)
		count += 4;
	count += (count / 64);
	*rez = ft_strnew_u(count);
	return (count);
}

ssize_t		u_allocate(t_list *tmp, unsigned char **line)
{
	t_list			*beg;
	size_t			i;
	size_t			c;

	c = 0;
	beg = tmp;
	while (beg)
	{
		c += beg->content_size;
		beg = beg->next;
	}
	(*line) = ft_memalloc(c + 1);
	if (*line == NULL)
		return (-1);
	i = 0;
	while (tmp)
	{
		ft_memcpy(&(*line)[i], tmp->content, tmp->content_size);
		i += (tmp->content_size);
		tmp = tmp->next;
	}
	return (c);
}

int			find_pos(char *str, char c)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

uint8_t		*ft_strnew_u(size_t size)
{
	return (uint8_t *)(ft_memalloc(size + 1));
}
