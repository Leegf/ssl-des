/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ssl_des.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 15:35:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/03/06 13:48:16 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SSL_DES_H
# define SSL_DES_H
# include "libft/includes/ft_printf.h"
# include <fcntl.h>

# define MASK_D (uint64_t)0b11111111111111111111111111111111
# define MASK_8_B (uint64_t)0b11111111
# define MASK_6_B (uint64_t)0b111111
# define MASK_COL (uint64_t)0b1111
# define MASK_KEY (uint64_t)0b1111111111111111111111111111
# define MASK_SH1 (uint64_t)0b1
# define MASK_SH2 (uint64_t)0b11

typedef struct		s_cmnds
{
	char			*s;
	int				(*ciph)(char **, int);
}					t_cmnds;

# define NUM_C 7
# define IN_SIZE 42
# define MASK_64_1 16515072
# define MASK_64_2 258048
# define MASK_64_3 4032
# define MASK_64_4 63

typedef struct		s_input
{
	unsigned char	*s;
	size_t			size;
}					t_input;

typedef struct		s_options
{
	int				d;
	int				in;
	int				a;
	uint8_t			*v;
	uint8_t			*hex;
	int				out;
	uint64_t		bl;
}					t_options;

typedef struct		s_out
{
	size_t		p;
	uint8_t		*buf;
}					t_out;

int					make_base64(char **av, int ac);
int					make_des_cbc(char **av, int ac);
t_input				*read_input(int fd);
void				initialize_options(t_options *ops);
void				read_options(char **av, t_options *ops, int ac);
void				error_options(char *s);
void				read_fd(char **av, t_options *ops, int ac, int *i);
void				read_out(char **av, t_options *ops, int ac, int *i);
void				read_hex(char **av, t_options *ops, int ac, int *i);
void				read_v(char **av, t_options *ops, int ac, int *i);
void				encrypt_base64(t_input *inp, t_options options, char *base);
size_t				buf_o(size_t hm, uint8_t **rez);
void				initialize_vrs_ffs_norm(ssize_t *k, size_t *count,
											size_t *hm);
void				make_int_from_octets(size_t *count, unsigned int *ui_buf,
										t_input inp);
void				general_case(uint32_t ui_buf, char *base, uint8_t *out_buf,
								ssize_t *k);
void				two_letters_left_case(uint32_t ui_buf, char *base,
										uint8_t *out_buf, ssize_t k);
void				three_letters_left_cas(uint32_t ui_buf, char *base,
										uint8_t *out_buf, ssize_t k);
void				decrypt_base64(t_input *inp, t_options options, char *base);
ssize_t				u_allocate(t_list *tmp, unsigned char **line);
int					find_pos(char *str, char c);
uint8_t				*ft_strnew_u(size_t size);
int					ft_is_hex(uint8_t *s);
void				error_hex(const char *s);
int					make_des_ecb(char **av, int ac);
void				initialize_base(char *tmp);
size_t				ft_strlenuu(uint8_t *s);
uint8_t				*ft_strdupu(uint8_t *s1);
uint64_t			ft_atoh(uint8_t *s);
uint8_t				*transform_to_uint_8(uint64_t rez);
size_t				buf_o_d(t_input hm, uint8_t **out);
void				make_int_from_sixlets(size_t *count, uint32_t *ui_buf,
										t_input inp, char *base);

void				free_and_write(t_input *inp, t_options options, t_out *out);

/*
** Permutation funcs.
*/
void				first_permutation_of_key(uint64_t *rez);
void				permutate_key(int arr[48], uint64_t *key);
void				keys_permutation(uint64_t keys[16]);
void				permutate_r(uint64_t *rez);
void				finelest_permutation(uint64_t *rez);
void				permutation_after_sboxes(uint64_t *rez);
void				first_permutation_of_data(uint64_t *rez);

/*
** Options or input related funcs
*/

void				check_options_des(t_options *options);
void				checking_upon_null_input(t_input *inp, uint64_t keys[16],
											t_list **head);
void				initialize_rez(uint64_t *rez, uint8_t tmp[8]);
void				fill_w_pad(uint8_t tmp[8], int diff, t_input *inp,
							size_t *pos);
void				output_des(t_list **head, t_options options, t_input *inp);
void				output_des3(t_list **head, t_options options, t_input *inp);
void				prepare_the_v(t_options *options);
void				i_luv_norm(t_input *inp, uint64_t keys[16], t_list **head,
							size_t *pos);
void				check_options_des_cbc(t_options *options);
void				check_options_des3(t_options *options, uint64_t hex_3[4]);

/*
** Keys-forming funcs.
*/

void				form_keys(t_options options, uint64_t keys[16]);
void				creating_key_pairs(uint64_t rez, uint64_t keys[16]);
void				prepare_the_key3(t_options *options, uint64_t hex_3[4],
									int index, size_t len);
void				form_keys3(uint64_t keys[16], uint64_t hex_3[4], int i);

/*
** encrypting-decrypting funcs.
*/

void				encrypt_des(t_input *inp, t_options options,
								uint64_t keys[16], t_list **head);
uint8_t				*encrypt_d_c(uint64_t keys[16], uint8_t tmp[9],
									t_options *options);
void				decrypt_des(t_input *inp, t_options options,
								uint64_t keys[16], t_list **head);
uint8_t				*encrypt_d(uint64_t keys[16], uint8_t tmp[9]);
uint64_t			function_f(uint64_t key, uint64_t r);
uint64_t			sbox_it(uint64_t chunk, int count);
void				make_it_butiful_decryption(uint8_t	**rez, ssize_t *hm);
void				prepare_the_key(t_options *options);
void				check_whether_base64_is_on(t_input *inp, t_options options);
void				fill_tmp(uint8_t tmp[9], t_input *inp, size_t *pos);
uint8_t				*decrypt_d(uint64_t keys[16], uint8_t tmp[9]);
uint8_t				*decrypt_d_c(uint64_t keys[16], uint8_t tmp[9],
									t_options *options, uint64_t tmp2);
void				decrypt_des_c(t_input *inp, t_options *options,
								uint64_t keys[16], t_list **head);
void				initial_vector(t_options *options, uint64_t *rez,
								uint64_t tmp, int flag);

/*
** des3 functions.
*/

int					make_des3_ecb(char **av, int ac);
void				make_it_butiful_decryption(uint8_t	**rez, ssize_t *hm);
void				encrypt_des3(t_input *inp, uint64_t keys[16],
								t_list **head);
int					make_des3_cbc(char **av, int ac);
uint8_t				*ft_strdupu_v(uint8_t *s1);
void				checking_upon_null_input_dc(t_input *inp,
							uint64_t keys[16], t_list **head, uint8_t **rez);
void				decrypt_des3(t_input *inp, t_options options,
								uint64_t keys[16], t_list **head);
void				des3_d(t_options options, uint64_t keys[16], t_input *hm,
						uint64_t hex_3[4]);
void				des3_e(t_options options, uint64_t keys[16], t_input *hm,
						uint64_t hex_3[4]);
void				encrypt_des_cbc3(t_input *inp, t_options *options,
									uint64_t keys[16], t_list **head);
void				decrypt_des_cbc3(t_input *inp, t_options *options,
									uint64_t keys[16], t_list **head);
void				output_des_cbc3(t_list **head, t_options options,
									t_input *inp);
void				des3_d_cbc(t_options *options, uint64_t keys[16],
							t_input *hm, uint64_t hex_3[4]);
void				des3_e_cbc(t_options *options, uint64_t keys[16],
							t_input *hm, uint64_t hex_3[4]);
void				for_the_sake_of_norm(t_options *options, uint8_t *tmp);

#endif
